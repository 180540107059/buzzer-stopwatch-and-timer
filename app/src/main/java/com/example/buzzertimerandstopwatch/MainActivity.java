package com.example.buzzertimerandstopwatch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import java.util.Timer;

import adapter.ViewPagerAdapter;
import fragment.BuzzerFragment;
import fragment.StopwatchFragment;
import fragment.TimerFragment;

public class MainActivity extends AppCompatActivity {

    public ViewPagerAdapter viewPagerAdapter;
    public ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(getSupportActionBar() != null){
            getSupportActionBar().hide();
        }
        bindFragments();
    }

    private void bindFragments() {

        viewPager = findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);

    }
}