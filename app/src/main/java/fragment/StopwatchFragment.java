package fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.os.TestLooperManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.buzzertimerandstopwatch.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StopwatchFragment extends Fragment {

    Button start , reset , lap;

    long millieSecondTime , startTime , timeTemp , updateTime = 0L;

    TextView timer;

    int second , minute , millisecond;

    Chronometer chronometer;
    long pause;

    ListView lstLap;

    String[] listElements = new String[] {  };

    List<String> listElementsArrayList ;

    ArrayAdapter<String> adapter ;

    Handler handler;

    Boolean isTimeRunning = false;
    public StopwatchFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_stopwatch, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        start = view.findViewById(R.id.btnStart);
        reset = view.findViewById(R.id.btnReset);
        lap = view.findViewById(R.id.btnLap);
        lstLap = view.findViewById(R.id.lstLap);
        chronometer = view.findViewById(R.id.chronometer);
        timer = view.findViewById(R.id.tvTimer);
        handler = new Handler();

        setAdapter();
        bindEvent();
    }
    private void setAdapter(){

        listElementsArrayList = new ArrayList<String>(Arrays.asList(listElements));

        adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1,
                listElementsArrayList
        );

        lstLap.setAdapter(adapter);
    }

    private void bindEvent() {

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isTimeRunning){
                    pauseChronometer();
                }else {
                    startChronometer();
                }

            }
        });

        lap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLap();
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetChronometer();
            }
        });

    }
    private void getLap() {

        long elapsedMillis = 0;
        elapsedMillis += millieSecondTime;
        handler.removeCallbacks(runnable);
        int second , minute;

        second = (int)(elapsedMillis / 1000);
        minute = second / 60;
        second = second % 60;
        millisecond = (int) updateTime % 1000;

        String time = String.format("%01d" , minute) + " : " + String.format("%02d" , second) + ":" + String.format("%03d" , millisecond);
        listElementsArrayList.add(time);
        adapter.notifyDataSetChanged();
        handler.postDelayed(runnable , 0 );

    }
    private void resetChronometer() {

        millieSecondTime = 0L ;
        startTime = 0L ;
        timeTemp = 0L ;
        updateTime = 0L ;
        second = 0 ;
        minute = 0 ;
        millisecond = 0 ;
        timer.setText("00:00:00");
        isTimeRunning = false;
        start.setText("Start");
        listElementsArrayList.clear();
        adapter.notifyDataSetChanged();
        lap.setVisibility(View.GONE);
    }
    public void pauseChronometer(){

        timeTemp += millieSecondTime;
        handler.removeCallbacks(runnable);
        isTimeRunning = false;
        start.setText("Resume");
        reset.setVisibility(View.VISIBLE);
    }
    public void startChronometer(){
        startTime = SystemClock.uptimeMillis();
        handler.postDelayed(runnable , 0 );
        start.setText("Pause");
        reset.setVisibility(View.INVISIBLE);
        lap.setVisibility(View.VISIBLE);
        isTimeRunning = true;

    }

    public Runnable runnable = new Runnable() {
        @Override
        public void run() {
            millieSecondTime = SystemClock.uptimeMillis() - startTime;
            updateTime = timeTemp + millieSecondTime;
            second = (int) updateTime / 1000;
            minute = second / 60;
            second = second % 60;
            millisecond = (int) updateTime % 1000;

            String timerFormat = String.format("" + minute + ":" + String.format("%02d" , second) + ":" + String.format("%03d" , millisecond));

            timer.setText(timerFormat);
            handler.postDelayed(this , 0);
        }
    };
}
