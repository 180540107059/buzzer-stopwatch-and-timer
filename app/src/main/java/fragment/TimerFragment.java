package fragment;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.buzzertimerandstopwatch.R;

import java.util.Locale;

public class TimerFragment extends Fragment {

    int minuteToConvert , secondToConvert;
    TextView tvCountdown;
    Button start , reset;

    long startingTime;
    long timeLeft;

    NumberPicker np_minutes , np_seconds;

    Boolean isTimerRunning = false;
    Boolean flag = true;

    CountDownTimer countDownTimer;

    public TimerFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_timer, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvCountdown = view.findViewById(R.id.tvTimer);
        start = view.findViewById(R.id.btnStart);
        reset = view.findViewById(R.id.btnReset);
        np_minutes = view.findViewById(R.id.npMinutes);
        np_seconds = view.findViewById(R.id.npSeconds);

        pickTime();
        bindEvent();
    }

    private void bindEvent() {

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isTimerRunning){
                    pauseTimer();
                }else {
                    initializeTime();
                    Log.d("start in start method","" + startingTime);
                    Log.d("timeLeft start method", "" + timeLeft);
                    startTimer();
                }
            }
        });


        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTimer();
            }
        });

        updateCountdownText();
    }

    void pauseTimer(){
        countDownTimer.cancel();
        isTimerRunning = false;
        start.setText("Start");
        reset.setVisibility(View.VISIBLE);
    }

    void startTimer(){

        countDownTimer = new CountDownTimer(timeLeft , 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeft = millisUntilFinished;
                updateCountdownText();
            }

            @Override
            public void onFinish() {
                isTimerRunning = false;
                timeLeft = 0;
                start.setText("Start");
                start.setVisibility(View.VISIBLE);
                reset.setVisibility(View.INVISIBLE);
                pickTime();
                initializeTime();
            }
        }.start();

        isTimerRunning = true;
        start.setText("Pause");
        reset.setVisibility(View.INVISIBLE);
    }

    void resetTimer(){
        timeLeft = 0;
        updateCountdownText();
        reset.setVisibility(View.INVISIBLE);
        start.setVisibility(View.VISIBLE);
        initializeTime();
    }

    private void initializeTime() {

        startingTime = convertInMilli(minuteToConvert , secondToConvert);
        timeLeft = startingTime;
        Log.d("startingTime" , ""+startingTime);
        Log.d("timeLeft" , ""+timeLeft);

    }

    void updateCountdownText(){

        int minutes = (int) (timeLeft / 1000) / 60;
        int seconds = (int) (timeLeft / 1000) % 60;
        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
        tvCountdown.setText(timeLeftFormatted);

    }

    Long convertInMilli(int minute, int second) {
        long millisecond;
        millisecond = minuteToConvert*60000 + secondToConvert*1000;
        Log.d("millisecond","" + millisecond);
        return millisecond;
    }

    private void pickTime(){

        if(np_minutes != null){

            np_minutes.setValue(0);
            np_minutes.setMinValue(0);
            np_minutes.setMaxValue(60);
            np_minutes.setWrapSelectorWheel(true);
            np_minutes.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    minuteToConvert = newVal;
                    Log.d("Minutes to convert" , "" + minuteToConvert);
                }
            });

        }

        if(np_seconds != null){

            np_seconds.setValue(0);
            np_seconds.setMinValue(0);
            np_seconds.setMaxValue(59);
            np_seconds.setWrapSelectorWheel(true);
            np_seconds.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    secondToConvert = newVal;
                    Log.d("Seconds" , "" + newVal);
                }
            });
        }

    }
}
