package fragment;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.buzzertimerandstopwatch.R;

public class BuzzerFragment extends Fragment {

    TextView clock , siren , piano , beep , whistle , schoolBell , train;

    CountDownTimer countDownTimer;

    boolean isSoundPlaying = false;

    MediaPlayer mp;

    private TextView textView;

    public BuzzerFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_buzzer, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        clock = (TextView) view.findViewById(R.id.tvClock);
        siren = (TextView) view.findViewById(R.id.tvSiren);
        piano = (TextView) view.findViewById(R.id.tvPiano);
        beep = (TextView) view.findViewById(R.id.tvBeep);
        whistle = (TextView) view.findViewById(R.id.tvWhistle);
        schoolBell = (TextView) view.findViewById(R.id.tvSchoolBell);
        train = (TextView) view.findViewById(R.id.tvTrain);

        playSound();
    }

    private void playSound() {

        clock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableBtn();
                mp = MediaPlayer.create(getContext() , R.raw.piano);
                mp.start();

                countDownTimer = new CountDownTimer(3000 , 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        mp.stop();
                        enableBtn();
                    }
                }.start();
            }
        });

        siren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableBtn();
                mp = MediaPlayer.create(getContext(), R.raw.siren);
                mp.start();

                countDownTimer = new CountDownTimer(3000 , 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        mp.stop();
                        enableBtn();
                    }
                }.start();
            }
        });

        piano.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableBtn();
                mp = MediaPlayer.create(getContext() , R.raw.piano);
                mp.start();

                countDownTimer = new CountDownTimer(3000 , 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        mp.stop();
                        enableBtn();
                    }
                }.start();
            }
        });

        beep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableBtn();
                mp = MediaPlayer.create(getContext() , R.raw.beep);
                mp.start();

                countDownTimer = new CountDownTimer(3000 , 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        mp.stop();
                        enableBtn();
                    }
                }.start();
            }
        });

        whistle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableBtn();
                mp = MediaPlayer.create(getContext() , R.raw.whistle);
                mp.start();

                countDownTimer = new CountDownTimer(3000 , 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        mp.stop();
                        enableBtn();
                    }
                }.start();
            }
        });

        schoolBell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableBtn();
                mp = MediaPlayer.create(getContext(), R.raw.school_bell);
                mp.start();

                countDownTimer = new CountDownTimer(3000 , 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        mp.stop();
                        enableBtn();
                    }
                }.start();
            }
        });

        train.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableBtn();
                mp = MediaPlayer.create(getContext() , R.raw.train);
                mp.start();

                countDownTimer = new CountDownTimer(3000 , 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        mp.stop();
                        enableBtn();
                    }
                }.start();
            }
        });
    }
    void disableBtn(){
        clock.setEnabled(false);
        siren.setEnabled(false);
        piano.setEnabled(false);
        beep.setEnabled(false);
        whistle.setEnabled(false);
        schoolBell.setEnabled(false);
        train.setEnabled(false);
    }
    void enableBtn(){
        clock.setEnabled(true);
        siren.setEnabled(true);
        piano.setEnabled(true);
        beep.setEnabled(true);
        whistle.setEnabled(true);
        schoolBell.setEnabled(true);
        train.setEnabled(true);
    }
}
